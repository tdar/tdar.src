package org.tdar.core.bean.resource.file;

/**
 * abstraction for things that have a file-extension
 * 
 * @author abrin
 * 
 */
public interface HasExtension {

    public String getExtension();
}

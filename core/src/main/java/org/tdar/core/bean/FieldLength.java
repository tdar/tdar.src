package org.tdar.core.bean;

public interface FieldLength {

    final int FIELD_LENGTH_5 = 5;
    final int FIELD_LENGTH_25 = 25;
    final int FIELD_LENGTH_32 = 32;
    final int FIELD_LENGTH_50 = 50;
    final int FIELD_LENGTH_100 = 100;
    final int FIELD_LENGTH_128 = 128;
    final int FIELD_LENGTH_254 = 254;
    final int FIELD_LENGTH_255 = 255;
    final int FIELD_LENGTH_500 = 500;
    final int FIELD_LENGTH_512 = 512;
    final int FIELD_LENGTH_1024 = 1024;
    final int FIELD_LENGTH_2048 = 2048;
    final int FIELD_LENGTH_5000 = 5_000;
}

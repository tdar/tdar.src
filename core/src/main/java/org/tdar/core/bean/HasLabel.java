package org.tdar.core.bean;

/**
 * Abstracts beans that have labels for display.
 * 
 * @author abrin
 * 
 */
public interface HasLabel {

    String getLabel();
}

package org.tdar.core.bean;

public interface Localizable {

    String getLocaleKey();
}
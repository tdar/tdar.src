package org.tdar.core.parser;

import java.util.List;

import org.tdar.core.bean.resource.OntologyNode;

public interface OntologyParser {

    List<OntologyNode> generate();

}

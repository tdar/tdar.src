# Adding a custom JS or CSS file
------------------------------------------------------------
1. Create javascript or css file -- /js /css
2. add js or css to src/main/resources/wro.xml
3. restart tDAR


# Adding a new Javascript package:
------------------------------------------------------------
1. edit the bower.json file manually or use bower to add the library to our bower package
2. bower will then add the files into src/main/webapp/components/
3. commit the files to mercurial
4. add the production versions of js and css files src/main/resources/wro.xml
5. restart tDAR


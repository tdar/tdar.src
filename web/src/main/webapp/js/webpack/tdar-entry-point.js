window.$ = window.jQuery = require('jquery'); // or import $ from 'jquery';
const TDAR = require("JS/tdar.master.js");

require("../../css/tdar-svg.css");
require("../../css/tdar-bootstrap.css");
require("../../css/tdar.dashboard.css");
require("../../css/famfamfam.css");
require("../../css/tdar-svg.css");
require("../../css/tdar.homepage.css");
require("../../css/tdar.d3tree.css");
require("../../css/tdar.c3graph.css");
require("../../css/tdar.leaflet.css");
require("../../css/tdar.worldmap.css");
require("../../css/tdar.datatablemapping.css");
require("../../css/tdar.sprites.css");
require("../../css/tdar.datatables.css");
require("../../css/tdar.searchresults.css");
require("../../css/tdar.invoice.css");
require("../../css/tdar-integration.css");

TDAR.main();
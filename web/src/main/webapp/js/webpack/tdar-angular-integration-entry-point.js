window.$ = window.jQuery = require('jquery'); // or import $ from 'jquery';

import "../data-integration/app.js";
import "../data-integration/ng-IntegrationController.js";
import "../data-integration/ng-IntegrationModel.js";
import "../data-integration/ng-IntegrationModalDialogController.js";
import "../data-integration/ng-IntegrationDataService.js";
import "../data-integration/ng-custom-directives.js";
import "../data-integration/ng-IntegrationCustomFilters.js";
import "../data-integration/ng-IntegrationValidationService.js";
import "../data-integration/ng-DatatableDirective.js";
